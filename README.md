# Example dataset for the CurvedTM package

## About
This is an example dataset of an everting wing disk of D. melanogaster to be used with the [CurvedTM Python package](https://gitlab.pks.mpg.de/paijmans/CurvedTM). It was created by N. Dye and J. Fuhrmann from the [Dye/Eaton group](https://www.mpi-cbg.de/de/forschungsgruppen/uebersicht/dye-eaton/) at the MPI-CBG. Database created using [Tissue Miner](https://github.com/mpicbg-scicomp/tissue_miner).

The data set contains 2 files and a directory
   - 20161110_116hrAEL_400nM20E.sqlite - SQLite database containing vertex positions, cell tracking and network topology, created by Tissue Miner.
   - vertex_scaling.txt - Scaling dimensions for convering pixels to micrometers.
   - heightmaps/ - Directory with 'tif' files representing the tissue heightmaps for every frame of the movie.


## Download
The dataset can be downloaded as a [zip file](https://gitlab.pks.mpg.de/paijmans/CurvedTM_dataset/-/archive/master/CurvedTM_dataset-master.zip) or by cloning the repository from the PKS GitLab server using 
```none
git clone https://gitlab.pks.mpg.de/paijmans/CurvedTM_dataset.git
```

## Usage
Download and extract the zip file to a directory named 'MovieData'. Then, in the Jupyter Notebook, set the path variabe `datapath_tissue_miner` to the directory you just created and run the notebook.